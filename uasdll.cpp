#include <iostream>
#include <cstring>

struct song {
	char title[50];
	char artist[50];
	int duration;
	struct song* next, *prev;
};

struct song *head=NULL, *tail=NULL;

void addSong(char title[50], char artist[50], int duration) {
	struct song *newSong;
	newSong = new song;

	std::strcpy(newSong->title, title);
	std::strcpy(newSong->artist, artist);
	newSong->duration = duration;

	if (head == NULL) {
		head = newSong;
		tail = newSong;
		newSong->prev = newSong;
		newSong->next = newSong;
	} else {
		if (duration < head->duration) {
			head->prev = newSong;
			newSong->next = head;
			head = newSong;
			newSong->prev = tail;
			tail->next = newSong;
		} else if (duration > tail->duration) {
			tail->next = newSong;

			newSong->prev = tail;
			newSong->next = head;

			tail = newSong;
		} else {
			struct song *currentSong;
			currentSong = head;

			do {
				if (duration < currentSong->duration) {
					currentSong->prev->next = newSong;
					newSong->prev = currentSong->prev;
					currentSong->prev = newSong;
					newSong->next = currentSong;
					break;
				}
				currentSong = currentSong->next;
			} while(currentSong != head);
		}
	}
}

void displayPlaylist() {
	if (head == NULL) {
		std::cout << "Playlist is empty." << std::endl;
		return;
	}

	struct song *currentSong;
	currentSong = head;

	std::cout << "Current Playlist : " << std::endl;

	do {
		std::cout << currentSong->title << " - " << currentSong->artist << " : " << currentSong->duration << " seconds" << std::endl;
		currentSong = currentSong->next;
	} while (currentSong != head);
}

int main() {
	char titles[20][50] = {
		"Ditto",
		"New Genesis",
		"The Foundations of Decay",
		"Alum",
		"Bintang Kehidupan"
	};

	char artists[20][50] = {
		"NewJeans",
		"Ado",
		"My Chemical Romance",
		"Gilga Sahid",
		"Nike Ardilla"
	};

	int durations[20] = {334, 238, 361, 330, 346};

	for(int i=0; i < 5; i++)
		addSong(titles[i], artists[i], durations[i]);

	char addNewSong;

	do {
		displayPlaylist();

		std::cout << "Do you want to add new song to the playlist? (Y/N) : "; std::cin >> addNewSong;

		if (addNewSong == 'Y' || addNewSong == 'y') {
			char title[50], artist[50];
			int duration;

			std::cin.ignore();
			std::cout << "Enter song title: ";
			std::cin.getline(title, sizeof(title));

			std::cout << "Enter artist name: ";
			std::cin.getline(artist, sizeof(artist));

			std::cout << "Enter song duration (in seconds): "; std::cin>>duration;

			addSong(title, artist, duration);

		}
	} while (addNewSong == 'Y' || addNewSong == 'y');

	return 0;
}